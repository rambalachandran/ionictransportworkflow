"""
Date: June 05, 2015
Objective: Read through the free energy data in all the sub-directories in the Dopant directory
"""
import os, sys
import shutil
from popen2 import popen2
import time
import math
import json
import re
import matplotlib.pyplot as plt
import numpy as np
from pprint import pprint
import pymatgen as mg
from pymatgen.core.structure import Structure

if __name__ == '__main__':
    #1. DATA GENERATION & SEPARATION
    folderPath = '/Users/kw1/Documents/ResearchProjects/IonicConductivity/Data/Perovskites/Analysis/DopantClustering/VaspData/Dopants'
    jsonFile = 'freeEnergy.json'
    
    with open(os.path.join(folderPath,jsonFile)) as inFile:
	inputData = json.load(inFile)
	
    dirName = inputData["inputPath"]
    fileName = inputData["inputFile"]
    outFileName = inputData["outFileName"]
    
    output, input = popen2('bash')
    jobstring = """find %s -type f -name %s
    """%(dirName,fileName)
    input.write(jobstring)
    input.close()

    fileList = output.read().split()
    energyValDict = {}
    
    for file in fileList:
	energyValList = []
	systemType = re.split('/',file)[-2]
	with open(file, 'r') as f:
	    for line in f:
		lineVal = line.split()
		pathVal = re.split('/', lineVal[0])
		energyVal = lineVal[3]
		dopant = [s for s in pathVal if 'M_' in s][0]
		if 'static_01' in pathVal:
		    energyValList.append([systemType, dopant, energyVal])
	energyValDict[systemType] = energyValList

    for key,val in energyValDict.iteritems():
	#print key, '\n', pprint(val), '\n\n'
	systemType = key
	if (systemType == 'bzo_sc333_dopant'):
	    freeEnergy_Dry_NoCluster = [float(s[2]) for s in val]
	if (systemType == 'bzo_sc333_dopant_1H'):
	    Dopants_NoCluster = [s[1] for s in val]
	    freeEnergy_Wet_NoCluster = [float(s[2]) for s in val]
	    indexVal_NoCluster = Dopants_NoCluster.index("M_Tl")
	    del Dopants_NoCluster[indexVal_NoCluster]
	    del freeEnergy_Wet_NoCluster[indexVal_NoCluster]
	if (systemType == 'bzo_sc333_dopantcluster'):
	    Dopants_Cluster_v1 = [s[1] for s in val]
	    freeEnergy_Dry_Cluster = [float(s[2]) for s in val]
	    index_Cluster = Dopants_Cluster_v1.index('M_Tl')
	    del Dopants_Cluster_v1[index_Cluster]
	    del freeEnergy_Dry_Cluster[index_Cluster]
	if (systemType == 'bzo_sc333_dopantcluster_1H'):
	    Dopants_Cluster_v2 = [s[1] for s in val]
	    freeEnergy_Wet_Cluster = [float(s[2]) for s in val]
	    index_Cluster = Dopants_Cluster_v2.index('M_Tl')
	    del Dopants_Cluster_v2[index_Cluster]
	    del freeEnergy_Wet_Cluster[index_Cluster]
	    
		    
    energyDiff_NoCluster = (np.asarray(freeEnergy_Dry_NoCluster)+ -0.11082602E+01) - np.asarray(freeEnergy_Wet_NoCluster)
    energyDiff_Cluster = (np.asarray(freeEnergy_Dry_Cluster)+ -0.11082602E+01) - np.asarray(freeEnergy_Wet_Cluster)

    xVal = np.arange(0,len(Dopants_NoCluster),1)
    plt.plot(xVal,energyDiff_NoCluster,'k-o', label='NO-Clustering')
    xVal = np.arange(0,len(Dopants_Cluster_v1),1)
    plt.plot(xVal,energyDiff_Cluster,'r-^', label='Clustering')
    
    plt.xticks(xVal,Dopants_Cluster_v1,rotation=45)
    axes = plt.gca()
    axes.set_xlim([-1,13])
    axes.set_ylim([3.4,6.4])
 
    plt.legend()
    plt.xlabel('Dopant Type')
    plt.ylabel('Adsorption Energy (eV)')
    plt.gcf().subplots_adjust(bottom=0.15)
    #plt.yscale('log')
    plt.savefig(os.path.join(dirName, 'FreeEnergy_Tot.eps'), format='eps', dpi=1000)
    plt.close()
    
    '''
    # print Dopants_NoCluster
    DopantAtoms = []
    for val in Dopants_NoCluster:
	atomType = re.split('_',val)[-1]
	dopantAtom = mg.Element(atomType)
	DopantAtoms.append(dopantAtom)
	
    # print DopantAtoms
    sortType = 'ionic_radii'
    sortNoCluster=zip(DopantAtoms, energyDiff_NoCluster)
    sortCluster=zip(DopantAtoms, energyDiff_Cluster)
    if (sortType == 'X'):
	sortNoCluster.sort(key=lambda (a,b):a.X)
	sortCluster.sort(key=lambda (a,b):a.X)
    elif (sortType == 'ionic_radii'):
	sortNoCluster.sort(key=lambda (a,b):a.ionic_radii[3])
	sortCluster.sort(key=lambda (a,b):a.ionic_radii[3])
    print sortType, pprint(sortNoCluster), pprint(sortCluster)

    DopantAtom_NoCluster = [x.symbol for (x,y) in sortNoCluster]
    energyDiff_NoCluster = [y for (x,y) in sortNoCluster]
    DopantAtom_Cluster = [x.symbol for (x,y) in sortCluster]
    energyDiff_Cluster = [y for (x,y) in sortCluster]
    energyDiff_Tot = [a-b for (a,b) in zip (energyDiff_Cluster, energyDiff_NoCluster)]
    print DopantAtom_NoCluster, energyDiff_NoCluster, energyDiff_Cluster
    if (sortType == 'X'):
	outgraphName = 'FreeEnergyDiff_SortElectronegativity.eps'
	xVal = np.arange(0,len(DopantAtom_NoCluster),1)
	plt.plot(xVal,energyDiff_NoCluster,'r-^', label='Adsorption Energy Diff')
    elif (sortType == 'ionic_radii'):
	outgraphName = 'FreeEnergyDiff_SortIonicRadii.eps'
	xVal = np.arange(0,len(DopantAtom_NoCluster),1)
	plt.plot(xVal,energyDiff_NoCluster,'b-s', label='Adsorption Energy Diff')

    plt.xticks(xVal,DopantAtom_NoCluster,rotation=45)
    axes = plt.gca()
    axes.set_xlim([-1,13])
    axes.set_ylim([4.5,5.1])
    plt.legend()
    plt.xlabel('Dopant Type')
    plt.ylabel('Energy (eV)')
    plt.gcf().subplots_adjust(bottom=0.15)
    plt.savefig(os.path.join(dirName, outgraphName), format='eps', dpi=1000)
    plt.close()
    '''
