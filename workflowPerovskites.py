#/usr/local/bin/python
import pymatgen as mg
import math
import pickle
import copy
from collections import namedtuple
from enum import Enum
import numpy as np
from pymatgen.io.smartio import read_structure, write_structure, \
    read_mol, write_mol

########## FIREWORKS CODE
from fireworks import Firework, LaunchPad, FWorker, ScriptTask
from fireworks.core.firework import Workflow
from fireworks_vasp.tasks import WriteVaspInputTask, VaspCustodianTask, VaspAnalyzeTask
from fireworks.core.rocket_launcher import rapidfire
from pymatgen.core.structure import Structure

##########
from pprint import pprint


# launchpad is used to add multiple workflows
launchpad = LaunchPad()
# if reset launchpad is necessary, uncomment the following line
#launchpad.reset('', require_password=False)

"""
1. This code can create cubic perovskite structures ABO3 (of symmetric supercell sizes nxnxn) employing pymatgen. 
2. This code can create substitutional dopants (tested in siteB for 1 dopant). The number, position of dopant hardcoded -- 1, max(siteB)
3. This code can create vacancies (tested for siteC for 1 vacancy). The number, position of vacancy is hardcoded -- 1, max(siteC)
4. This code can create simple interstitial atoms (tested for C-B bonds for one atom (pure), 2 atoms (doped)). The number, position of interstitials is hardcoded --
  a. Pure or Only Vacant defects --> [1, max(siteC)],
  b. Only Doped or Doped+Vacant defects --> [2, max(siteC-1), max(siteC-2)] where siteC-1 are sites whose the two nearest neighbors (for cubic perovskites) are only pure siteB, while siteC-2 are sites which have atleast one of the two nearest neighbors is a dopant atom.
5. The structures are saved as a dictionary in crystalStrucData. Each entry in crystalStrucData has a unique key associated with it whose format is given by structureKey.
"""

__author__ = "Ram Balachandran, Lin Lianshan, Ganesh Panchapakesan"
__version__ = "1.0.0"
__maintainer__ = "Ram Balachandran"
__email__ = "balachandraj@ornl.gov"
__date__ = "Feb,05 2015"
__status__ = "Development"


"""
##### URGENT
### STRUCTURE CREATION
#TODO: Export output crystal structure in json format to mongo database.
#TODO: Remove hardcoding on number and position of point defect sites.
### WORKFLOW
#TODO: set up the LaunchPad and reset it
#TODO: use a simple collection to simulate the loop, 
#TODO: with different siteA, siteB and dopant atoms, we can use 3 loops

##### NOT URGENT
#TODO: The inputCrystalInfo must be converted into an input class which can parse a json/xml input file
#TODO: create_crystal_structure must be a class with two derived classes - 1. pure structure & 2. point defects. The point defects class in turn will have many derived classes (substitutional dopant, vacancy, interstitials etc)
#TODO: Generalize the crystal structure classes to include non-cubic perovskites, spinels & flourite type structures (LWO)
"""



class atomSites(Enum):
    siteA = 0
    siteB = 1
    siteC = 2



def return_site_index(inputCrystalInfo,keyValue,siteName,noSites):
    """ Can return the atomic site index that needs to be doped (substituional)
or needs to be removed (Vacancy). Currently can only return the maximum of site id. Needs to be generalized
    """
    noCells=keyValue[5]
    noAtomSites = inputCrystalInfo["noAtomSites"]
    noAtoms = [0]* noAtomSites 
    atomsUnitCell = [0]*noAtomSites
    totalNoCells = 1
    siteIndex=atomSites[siteName]
    
    for i in noCells:
        totalNoCells*=i
    for key, val in inputCrystalInfo["atomComposition"].iteritems():
        site = atomSites[key]
        atomsUnitCell[site.value]=val
        noAtoms[site.value]=val*totalNoCells
    atomSiteIndex = []
    indexStartVal = 0
    for i in range(0,noAtomSites):
        atomSiteIndex.append(range(indexStartVal,indexStartVal+noAtoms[i]))
        indexStartVal+=noAtoms[i]
    return max(atomSiteIndex[siteIndex.value])

def return_lattice_constant(inputCrystalInfo,keyValue):
    ### HARDCODING ALERT-- 1. keyValue 2.oxidationStates
    noAtomSites = inputCrystalInfo["noAtomSites"]
    oxidationStates = [2, 4, -2]
    atoms = []
    ionicRadii = []
    for i in range(0, len(keyValue[1])): 
        atoms.append(mg.Element(keyValue[1][i]))
        ionicRadii.append(atoms[i].ionic_radii[oxidationStates[i]])
        
    if (inputCrystalInfo["latticeSystem"]== "cubic"):
        latticeVal1 = 2*(ionicRadii[0]+ionicRadii[1])/math.pow(3,1/2)
        latticeVal2 = 2*(ionicRadii[0]+ionicRadii[2])/math.pow(2,1/2)
        latticeConst = max(latticeVal1, latticeVal2)
    else:
        print "ERROR: Only Cubic Perovskite Currently Implemented"
        latticeConst = float('NaN')
        
    return latticeConst


#Code adopted from https://stackoverflow.com/questions/6802577/python-rotation-of-3d-vector
def rotation_matrix(axis, theta):
    """
    Return the rotation matrix associated with counterclockwise rotation about
    the given axis by theta radians.
    """
    axis = np.asarray(axis)
    theta = np.asarray(theta)
    axis = axis/math.sqrt(np.dot(axis, axis))
    a = math.cos(theta/2)
    b, c, d = -axis*math.sin(theta/2)
    aa, bb, cc, dd = a*a, b*b, c*c, d*d
    bc, ad, ac, ab, bd, cd = b*c, a*d, a*c, a*b, b*d, c*d
    return np.array([[aa+bb-cc-dd, 2*(bc+ad), 2*(bd-ac)],
                     [2*(bc-ad), aa+cc-bb-dd, 2*(cd+ab)],
                     [2*(bd+ac), 2*(cd-ab), aa+dd-bb-cc]])       

        
def create_pure_structure(inputCrystalInfo, structureKey, crystalStrucData):
    oxidationA = inputCrystalInfo["oxidationStates"]["siteA"]
    oxidationB = inputCrystalInfo["oxidationStates"]["siteB"]
    oxidationC = inputCrystalInfo["oxidationStates"]["siteC"]
    dopantAtoms = (" ", " ", " ")
    noDopants = (0, 0, 0)
    noVacancies = (0, 0, 0)
    noInterstitials = (0,)
    
    for siteA in inputCrystalInfo["basicAtoms"]["siteA"]:
        for siteB in inputCrystalInfo["basicAtoms"]["siteB"]:
            for siteC in inputCrystalInfo["basicAtoms"]["siteC"]:
                atomA = mg.Element(siteA)
                atomB = mg.Element(siteB)
                atomC = mg.Element(siteC)
                ionic_radiiA = atomA.ionic_radii[oxidationA]
                ionic_radiiB = atomB.ionic_radii[oxidationB]
                ionic_radiiC = atomC.ionic_radii[oxidationC]
                latticeSystem = inputCrystalInfo["latticeSystem"]
                latticeVal1 = 2*(ionic_radiiA+ionic_radiiB)/math.pow(3,1/2)
                latticeVal2 = 2*(ionic_radiiA+ionic_radiiC)/math.pow(2,1/2)
                latticeConst = max(latticeVal1, latticeVal2)
                lattice = mg.Lattice.cubic(latticeConst)
                for xCell in range (1, inputCrystalInfo["maxCells"][0]+1):
                    yCell = xCell
                    zCell = xCell
                    structure = mg.Structure(lattice, [siteA, siteB, siteC, siteC, siteC],
                                             [[0, 0, 0], [0.5, 0.5, 0.5], [0.5, 0.5, 0],
                                              [0.5, 0, 0.5], [0, 0.5, 0.5]])
                    if (xCell >1):
                        structure.make_supercell([xCell, yCell, zCell])
                    keyVal = structureKey((latticeSystem,), (siteA, siteB, siteC),
                                          dopantAtoms, noDopants, noVacancies,
                                          (xCell, yCell, zCell), noInterstitials)
                    crystalStrucData[keyVal] = structure
    del structure
                    

def create_substitutional_dopant_structure(inputCrystalInfo, structureKey, crystalStrucData):
    '''
    1. How to extract coordinates from an atom type or atomic index or get an index based on coordinates
    2. How to replace atomtype at an index in structure?
    3. Ideally this function (or future class) should be able to automatically create dopants based on dopantsite (lattice structure, symmetry etc), type of dopant atoms and no. of dopant atoms
    '''
    if (inputCrystalInfo["maxDopantAtoms"]["siteB"] > 0):
        dopantSite = "siteB"
        tempStrucDict = {}
        for noDopants in range(1,inputCrystalInfo["maxDopantAtoms"]["siteB"]+1):
            for siteDopant in inputCrystalInfo["dopantAtoms"]["siteB"]:
                atomDopant = mg.Element(siteDopant)
                for key, val in crystalStrucData.iteritems():
                    if (key[5][0] > 1):
                        structure=copy.deepcopy(val)
                        listKey=[list(i) for i in key] #How to use map to create this key?
                        dopantSiteIndex = return_site_index(inputCrystalInfo,key,
                                                            dopantSite,noDopants)
                        structure.replace(dopantSiteIndex,siteDopant)
                        listKey[2][1]=siteDopant
                        listKey[3][1]=noDopants
                        newKey=structureKey._make(tuple(i) for i in listKey)
                        tempStrucDict[newKey] = structure
        for key, val in tempStrucDict.iteritems():
            crystalStrucData[key] = val

def create_vacancy_defect_structure(inputCrystalInfo, structureKey, crystalStrucData):
    if (inputCrystalInfo["maxVacancies"]["siteC"]>0):
        vacancySite="siteC"
        tempStrucDict = {}
        for noVacancies in range(1,inputCrystalInfo["maxVacancies"]["siteC"]+1):
            for key, val in crystalStrucData.iteritems():
                if (key[5][0] > 1):
                    structure=copy.deepcopy(val)
                    vacancySiteIndex= [return_site_index(inputCrystalInfo,key,
                                                        vacancySite,noVacancies)] 
                    structure.remove_sites(vacancySiteIndex) #INPUT needs to be a list
                    listKey=[list(i) for i in key]
                    listKey[4][2]=noVacancies
                    newKey=structureKey._make(tuple(i) for i in listKey)
                    tempStrucDict[newKey]=structure

        for key, val in tempStrucDict.iteritems():
            crystalStrucData[key] = val

def create_interstitial_dopant_structure(inputCrystalInfo,structureKey,crystalStrucData):
    tempStrucDict = {}
    bondLength_OH = inputCrystalInfo["interstitialBondLength"]
    bondAngle = inputCrystalInfo["interstitialBondAngle"] #In Degrees
    rotateAxis = inputCrystalInfo["interstitialBondRotateAxis"]
    for key, val in crystalStrucData.iteritems():
        if (key[5][0] > 1):
            latticeConst = return_lattice_constant(inputCrystalInfo,key)
            neighborDistance  = 0.51*latticeConst
            structure = copy.deepcopy(val)
            listKey=[list(i) for i in key]
            
            if (key[3][1] == 0):
                #noInterstitials = 2
                #dopantAtom = True
                listKey[6][0] = 1
                neighborSiteType = key[1][1]
                                
            elif (key[3][1] > 0):
                #noInterstitials = 1
                listKey[6][0] = 1
                neighborSiteType = key[2][1]

            for atomIndexCnt, atom in enumerate(structure):
                if atom.species_string in neighborSiteType:
                    siteB_atom = atom
                    siteB_neighbors = [site for site in structure.get_neighbors(atom, neighborDistance, True)
                                       if site[0].species_string in ('O')]
                    
                    #neighborSite = [site for site in structure.get_neighbors(atom, neighborDistance, True)
                    #                if site[0].species_string in ('O')]
                    #siteB_info = [atom, atomIndexCnt,neighborSite]
                    break

            oxyAtom = siteB_neighbors[2][0]
            diffVec = np.asarray(siteB_atom.coords) - np.asarray(oxyAtom.coords)
            unit_diffVec = diffVec/np.linalg.norm(diffVec)
            bondVec = np.dot(rotation_matrix(rotateAxis,math.radians(bondAngle)), unit_diffVec)
            hydrogenCoords = (np.asarray(oxyAtom.coords) + bondLength_OH*bondVec).tolist()
            structure.append('H', hydrogenCoords,True,True)
            
            
            newKey=structureKey._make(tuple(i) for i in listKey)
            tempStrucDict[newKey] = structure

    for key, val in tempStrucDict.iteritems():
        crystalStrucData[key] = val

        
def create_crystal_structure(inputCrystalInfo, structureKey, crystalStrucData):
    create_pure_structure(inputCrystalInfo, structureKey, crystalStrucData)
    """ Create a pointdefect_structure class and the substitutional dopant, interstitial dopant & vacancies as derived classes """
    
    create_substitutional_dopant_structure(inputCrystalInfo, structureKey, crystalStrucData)
    create_vacancy_defect_structure(inputCrystalInfo, structureKey, crystalStrucData)
    create_interstitial_dopant_structure(inputCrystalInfo, structureKey, crystalStrucData)

    file=open('crystalStrucFile_Dopant_Vacancy.txt','wb')
    i=0 #Testing
    for key,val in crystalStrucData.iteritems():
        file.write("The keyVal is {}\n\n".format(key))
        file.write("The crystal structure is {}\n\n\n".format(crystalStrucData[key]))
        #if (key[6][0]>0):
        #    val.to("POSCAR","BZO_POSCAR"+str(i))
        #    i += 1
        
        #create fireworks in database
        
        create_fireworks(crystalStrucData[key], key) #COMMENTED FOR TESTING
        #write_structure(val,'DopedSystem.cif') #COMMENTED FOR TESTING
    file.close()
    #launch all the created fireworks, since we run in offline mode, this line is disabled
    #rapidfire(launchpad,FWorker())

def create_fireworks(structure, keyVal, viset='MPVaspInputSet', params={}, handlers=[], vasp_cmd=["mpirun","-np","16","/home/g5q/Codes/VASP/svn/PentaV/src/vasp.5.2.12/vasp.oic_opt"]):
    """create firework for each structure"""
    wf_name = structure.formula
    t1 = WriteVaspInputTask(structure=structure, vasp_input_set=viset, input_set_params=params)
    t2 = VaspCustodianTask(vasp_cmd=vasp_cmd, handlers=handlers)
    t3 = VaspAnalyzeTask()
    workflow=Firework([t1, t2, t3],name='FW-test-'+wf_name, spec=dict(keyVal._asdict()))
    launchpad.add_wf(workflow)
    

if __name__ == '__main__':
    inputCrystalInfo = {"noAtomSites" : 3,
                        "atomComposition" : {"siteA":1, "siteB":1, "siteC":3},
                        "latticeSystem" : ("cubic"),
                        "basicAtoms" : {"siteA":("Ba",),
                                        "siteB": ("Zr",),
                                        "siteC": ("O",)},
                        "dopantAtoms" : {"siteA":(" ",), "siteB":("Y",),
                                        "siteC":(" ",)},
                        "maxDopantAtoms" : {"siteA":0, "siteB":1, "siteC":0},
                        "maxVacancies" : {"siteA":0, "siteB":0, "siteC":1},
                        "maxCells": (2, 2, 2),
                        "oxidationStates" : {"siteA":2, "siteB":4, "siteC":-2, "dopant":3},
                        "maxInterstitials":1,
                        "interstitialBondLength" : 0.96,
                        "interstitialBondAngle" : 45,
                        "interstitialBondRotateAxis": [1,0,0]
                        }
                    
    
    structureKey = namedtuple("structureKey", "latticeSystem basicAtoms dopantAtoms noDopants noVacancies noCells noInterstitials")
    crystalStrucData = {}
    create_crystal_structure(inputCrystalInfo, structureKey, crystalStrucData)


