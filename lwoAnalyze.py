"""
Date: April 22, 2015
Objective:
A. INPUTS (in json file): #TODO change to mongodb file soon
   i. Dry LWO structure 
   ii. vacancy site info - index(175 in this case) or coordinates 
   iii. shell radius around vacancy site & atom of interest (O in this case)
   iv.  radius for atoms away from vacacy site & atom of interest (O in this case)
   v.  OH bond length
   vi. Criteria to bin atoms near (NONE) & away (no. of W coordination) from vacancy site
B. PROCESSING
 1. Create Hydrated Structure:
   i.   Add O to vacant site
   ii.  Add H to the O in vacant site(such that all H face the nearest O atoms)
   ii.  Bin O atoms away from vacant site based on their local coordination
   iii. Pick one O atom from each bin and add H (that points towards its neighboring O atom)
   iv. Create Hydrated structures by combining dry structure with the OH & H coordinates
   v.   Write out the POSCARS for all the files
 2. Submit VASP Jobs in Hopper (with Fireworks)
 3. Perform bader charge & bond valence analysis on the converged structure.
 4. Compute phonon modes and frequencies
 5. Ab-initio MD
 6. NEB
 7. KMC + Cluster Expansion

C. OUTPUTS
 1. Visualize the output structure in blender.
 2. Parse for ground state energy, store them (in csv/json format) & plot them

D. MISC
 1. Build an e-tag repository for the project
 
"""
import os, sys
import pymatgen as mg
from pymatgen.core.structure import Structure
import numpy as np
import csv
import itertools
import math
import simplejson as json
#from json import dumps

def calcAtomCoordination(site,crystalStruc,inputData):
    NoAtomCoordination = len([site2 for site2 in crystalStruc.get_neighbors(site,
							     inputData["radius"],
							     True)
			     if site2[0].species_string in ('W')])
    return NoAtomCoordination
    
if __name__ == '__main__':
    jsonFile = '/Users/kw1/Documents/myCodes/testData/LWO_Test/systemLWO.json'
    with open(jsonFile) as dataFile:  #COMMENT: sys.arg should replace jsonFile 
	inputData = json.load(dataFile)
    crystalStruc = Structure.from_file(os.path.join(inputData["inputPath"],
						    inputData["inputFile"]))
    crystalStruc.remove_sites(inputData["interstitialIndex"])
    radius = inputData["radius"]
    bondLength_OH = inputData["bondLength"]
    
    oxygenAwayFromVacancy = []
    MaxNoW = 2
    oxygenAwayAnalysis = []
    hydrogenNearVac = []
    hydrogenAwayFromVac = []

    hydrogenNearVacInfo =[] #FOR PRINTING INFO
    hydrogenAwayFromVacInfo =[] #FOR PRINTING INFO
    
    for i, vacancyIndex in enumerate(inputData["vacancyIndex"]):
	oxygenNearVacancy =[site for site in
			      crystalStruc.get_neighbors(crystalStruc[vacancyIndex],radius,True)
			      if site[0].species_string in inputData["analyzeAtom"]
			      ]

	for distanceFromVacancy in inputData["distFromVacancy"]:
	    for noW in range(0,MaxNoW+1):
		tempOxyList = []
		for index, site in enumerate(crystalStruc):
		    if (site.species_string in inputData["analyzeAtom"] and
			calcAtomCoordination(site,crystalStruc,inputData) == noW
			and site.distance(crystalStruc[vacancyIndex]) > distanceFromVacancy):
			#print index, distanceFromVacancy, noW, site.distance(crystalStruc[vacancyIndex])
			tempOxyList.append((site, site.distance(crystalStruc[vacancyIndex]),index))
		#oxygenAwayFromVacancy.append(tempOxyList)
		oxygenAwayFromVacancy.append(tempOxyList[0])
		
	    # neighborAtomsOxyAwayVac = [0]*len(oxygenAwayFromVacancy)
	    neighborAtomsOxyAwayVac = [[0 for x in range(MaxNoW+1)]
				       for x in range(len(oxygenAwayFromVacancy))]
	    print pprint(neighborAtomsOxyAwayVac)
	    for index, site in enumerate(oxygenAwayFromVacancy):
		for noW in range(0,MaxNoW+1):
		    tempOxyList = [site2 for site2 in
				   crystalStruc.get_neighbors(site[0],radius,True)
				   if (site2[0].species_string in inputData["analyzeAtom"]
				   and calcAtomCoordination(site2[0],crystalStruc,inputData) == noW)]
		    if tempOxyList:
			neighborAtomsOxyAwayVac[index][noW] = tempOxyList[0]
		

	print '\n\n\n', 'OxygenNearVacancy\n', pprint(oxygenNearVacancy)
	print '\n\n\n', 'oxygenAwayFromVacancy\n', pprint(oxygenAwayFromVacancy)
	print '\n\n\n', 'neighborAtomsOxyAwayVac\n', pprint(neighborAtomsOxyAwayVac)

	for index, site in enumerate(oxygenNearVacancy):	    
	    diffVec = np.asarray(site[0].coords) - np.asarray(crystalStruc[vacancyIndex].coords)
	    diffVecMag = math.sqrt(np.dot(diffVec,diffVec))
	    unit_diffVec = diffVec/diffVecMag
	    hydrogenCoords = (np.asarray(crystalStruc[vacancyIndex].coords) + bondLength_OH * unit_diffVec).tolist()
	    hydrogenNearVacInfo.append((hydrogenCoords, site[2], vacancyIndex))
	    #hydrogenNearVac.append(hydrogenCoords)

	print '\n\n\n', 'HydrogenNearVac\n', pprint(hydrogenNearVacInfo)
	
	for index, site in enumerate(oxygenAwayFromVacancy):
	    for noW in range(0,MaxNoW+1):
		if (neighborAtomsOxyAwayVac[index][noW] != 0):
		    diffVec = np.asarray(neighborAtomsOxyAwayVac[index][noW][0].coords) - np.asarray(site[0].coords)
		    diffVecMag = math.sqrt(np.dot(diffVec,diffVec))
		    unit_diffVec = diffVec/diffVecMag
		    hydrogenCoords = (np.asarray(site[0].coords) + bondLength_OH * unit_diffVec).tolist()
		    hydrogenAwayFromVacInfo.append((hydrogenCoords,
						    neighborAtomsOxyAwayVac[index][noW][2],
						    site[2]))
		    #hydrogenAwayFromVac.append(hydrogenCoords)

	print '\n\n\n', 'hydrogenAwayFromVac\n', pprint(hydrogenAwayFromVacInfo)

	# VASP IO
	
	if (inputData["write_vasp"]):
	    for index, coords in enumerate(hydrogenNearVacInfo):
		#print index, 'Hello'+str(index) , coords, coords[0]
		crystalStruc.append('H',coords[0],True)
		outFile = os.path.join (inputData["inputPath"], 'POSCAR_OH_in_Vac_'
					+str(coords[1])+'_'+
					str(coords[2])+'_id_'+str(index))
		crystalStruc.to("POSCAR", outFile)
		crystalStruc.remove_sites(inputData["interstitialIndex"])
	    
	    for index, coords in enumerate(hydrogenAwayFromVacInfo):
		crystalStruc.append('H',coords[0],True)
		outFile = os.path.join (inputData["inputPath"], 'POSCAR_H_near_O_'
					+str(coords[1])+'_'+
					 str(coords[2])+'_id_'+str(index))
		crystalStruc.to("POSCAR", outFile)
		crystalStruc.remove_sites(inputData["interstitialIndex"])

	    i=0
	    for coords1, coords2 in zip(hydrogenNearVacInfo,hydrogenAwayFromVacInfo):
		interstialIndex = inputData["interstitialIndex"][0]
		crystalStruc.append('H',coords1[0],True)
		crystalStruc.append('H',coords2[0],True)
		outFileName = 'POSCAR_H20_'+ 'Vac_'+str(coords1[1])+'_'+str(coords1[2]) + '_Oxy_'+str(coords2[1])+'_'+str(coords2[2])+'_id_'+str(i)
		#print outFileName
		outFile = os.path.join (inputData["inputPath"],outFileName)
		crystalStruc.to("POSCAR", outFile)
		crystalStruc.remove_sites([interstialIndex,interstialIndex+1])
		i += 1

		
    '''
					   tempOxyList = [(site, site.distance(crystalStruc[vacancyIndex]), index)
			       for index, site in enumerate(crystalStruc)
			       if (site.species_string in inputData["analyzeAtom"] 
			       )]
    if (site.species_string in inputData["analyzeAtom"] and
			   site.distance(crystalStruc[index]) > distFromVacancy
    and
			   len([site2 for site2 in crystalStruc.get_neighbors(site,radius, True) if site2[0].species_string in ('W')]) == noW
			   
    ##### TEST BEGINS  ####
    print inputData["analyzeAtom"], inputData["distFromVacancy"]
    #[64, 68, 108]]
    distFromVacancy = 4.0
    radius = 3.0
    print crystalStruc[108]
    for index in inputData["vacancyIndex"]:
	print '\n\n\n'
	pprint([site for site in crystalStruc
	       if (site.species_string in inputData["analyzeAtom"] and
	       site.distance(crystalStruc[index]) < radius)])
	print '\n\n\n'
	pprint([site for site in crystalStruc
		if (site.species_string in inputData["analyzeAtom"] and
		    site.distance(crystalStruc[index]) > distFromVacancy  and
		    len([site2 for site2 in crystalStruc.get_neighbors(site,radius, True) if site2[0].species_string in ('W')]) > 1
		)])
    
    ##### TEST ENDS  ####
    
    interstitialAtoms = {"NearVacancy" :[], "NotNearVacancy" : []}
    hydrogenNearVacancy = []
    hydrogenAwayFromVacancy = []
    # FIX ME: Don't use zip
    for vacancyIndex, radShell, dR in zip(inputData["vacancyIndex"],
					  inputData["radShellVac"], inputData["dRVac"]):
	neighborSite = crystalStruc.get_neighbors_in_shell(crystalStruc[vacancyIndex].coords, radShell, dR)
	for site in neighborSite:
	    if(site[0].species_string == inputData["analyzeAtom"]):
		diffVec = np.asarray(crystalStruc[vacancyIndex].coords) - np.asarray(oxyAtom[0].coords)
		unit_diffVec = diffVec/np.linalg.norm(diffVec)
		hydrogenCoords = site[0].coords + (inputData["bondLength"] * unit_diffVec).tolist()
		tempDict = {"H_coords" :hydrogenCoords, "BondingAtom" : site,  "bondLength": inputData["bondLength"], "vacancyIndex" : vacancyIndex}
		interstitialAtoms["NearVacancy"].append(tempDict)

    neighborSite = []
    site = ()
    for distFromVacancy, radShell, dR in zip(inputData["distFromVacancy"],
					     inputData["radShell"], inputData["dR"]):
	atomIndexCnt = 0
	for site in crystalStruc:
	    for vacancyIndex in inputData["vacancyIndex"]:
		if(site.species_string == inputData["analyzeAtom"] and site.distance(crystalStruc[vacancy])>distFromVacancy):
		    neighborSite = crystalStruc.get_neighbors_in_shell(site, radius, true)
    '''
